//TOF
#include <climits>

#include "Pythia8/Pythia.h"
using namespace Pythia8; 

/*****************************************************************************/
/* Event generator code (main program).                                      */
/*****************************************************************************/
int main(int argc, char *argv[])
{
  bool status;

  // Initialize random number generator with time(0).
  // Warning: will casue different results each run.
  //Rndm::init(0);

  // Declare Pythia object.
  Pythia* pythia = new Pythia();

  // Number of event, black hole charge, and inital state.
  (void)pythia->readString("Main:NumberOfEvents = 10000");
  if (argc == 2) {
    pythia->readString("Main:NumberOfEvents = " + (string)argv[1]);
  }

  // Set some switches.
  (void)pythia->readString("PartonLevel:all   = on");
  (void)pythia->readString("SLHA:readFrom = 1");
  (void)pythia->readString("SLHA:minMassSM = 0.0");

  // Initialize Pythia object.
  (void)pythia->readString("Beams:frameType = 4");
  (void)pythia->readString("Beams:LHEF = STRINGSfile.lhe");
  status = pythia->init();

  // Loop over events.
  if (status) {
    for (int i=0; i<pythia->mode("Main:numberOfEvents"); ++i) {
      // Generate event.
      status = pythia->next();
      if (!status) continue;
      // Event listing.
      if (i < pythia->mode("Next:numberShowEvent")) {
        pythia->info.list();
        //pythia->process.list();
        pythia->event.list();
      }
      if (pythia->info.atEndOfFile()) break;

    } // End of event loop.
  }
   
  // Termination.
  pythia->stat();
  
  long t = clock();
  double dt = (double)t;
  if (t < 0) dt += 2.0 * (double)LONG_MAX;
  long min = (long)((dt/(double)CLOCKS_PER_SEC) / 60.0);
  long sec = (long)(dt/(double)CLOCKS_PER_SEC) % 60;
  printf("\nProcessing time %ld min %ld sec\n",min,sec);

  if (status) {
    printf("Normal sucessful completion.\n\n");
  }
  return status;
}
/*****************************************************************************/
//EOF

