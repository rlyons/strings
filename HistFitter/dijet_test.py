"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Simple example configuration using a single bin and raw numbers           * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

################################################################
## In principle all you have to setup is defined in this file ##
################################################################

from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
from math import sqrt

from ROOT import gROOT
#gROOT.LoadMacro("./macros/AtlasStyle.C")
import ROOT
#ROOT.SetAtlasStyle()

#configMgr.doHypoTest=False
#configMgr.nTOYs=5000
configMgr.calculatorType=2
configMgr.testStatType=3
configMgr.nPoints=40
#configMgr.prun = True

configMgr.writeXML = False
configMgr.keepSignalRegionType = True

# First define HistFactory attributes
configMgr.analysisName = "mjj_dijet_test"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"
configMgr.outputFileName = "results/"+configMgr.analysisName+"_Output.root"

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 139 # Luminosity of input TTree after weighting
configMgr.outputLumi = 139 # Luminosity required for output histograms
configMgr.setLumiUnits("fb-1")

#configMgr.blindSR=False
#configMgr.useSignalInBlindedData=False 

wbins = [946, 976, 1006, 1037, 1068, 1100, 1133, 1166, 1200, 1234, 1269, 1305, 1341, 1378, 1416, 1454, 1493, 1533, 1573,1614, 1656, 1698, 1741, 1785, 1830, 1875, 1921, 1968, 2016, 2065, 2114, 2164, 2215, 2267, 2320, 2374, 2429, 2485, 2542, 2600, 2659, 2719, 2780, 2842, 2905, 2969, 3034, 3100, 3167, 3235, 3305, 3376, 3448, 3521, 3596, 3672, 3749, 3827, 3907, 3988, 4070, 4154, 4239, 4326, 4414, 4504, 4595, 4688, 4782, 4878, 4975, 5074, 5175, 5277, 5381, 5487, 5595, 5705, 5817, 5931, 6047, 6165, 6285, 6407, 6531, 6658, 6787, 6918, 7052, 7188, 7326, 7467, 7610, 7756, 7904, 8055, 8208, 8364, 8523, 8685, 8850, 9019, 9191, 9366, 9544, 9726, 9911, 10100, 10292, 10488, 10688, 10892, 11100, 11312, 11528, 11748, 11972, 12200, 12432, 12669, 12910, 13156]

nBins = len(wbins)-1
BINLOW = 0
BINWIDTH = 1

configMgr.cutsDict["SR"] = "1"

#-------------------------------------------
# List of samples and their plotting colours
#-------------------------------------------
def GetHist(file):
	data = open(file,"r")
	Hist = data.read().strip('\n').split(',')
	Histf = [float(a) for a in Hist]
	data.close()
	return Histf

path = "/strings/HistFitter/inputs"
systype = "histoSys"

bkgHist = GetHist(path + "/bkg_mjj_dijet.txt")
bkgError = GetHist(path + "/bkg_mjj_dijeterr.txt")
dataHist = GetHist(path + "/data_mjj_dijet.txt")
dataError = GetHist(path + "/data_mjj_dijeterr.txt")
strings7000Hist = GetHist(path + "/strings7000_mjj_dijet.txt")
strings7000Error = GetHist(path + "/strings7000_mjj_dijeterr.txt")
strings7500Hist = GetHist(path + "/strings7500_mjj_dijet.txt")
strings7500Error = GetHist(path + "/strings7500_mjj_dijeterr.txt")
strings8000Hist = GetHist(path + "/strings8000_mjj_dijet.txt")
strings8000Error = GetHist(path + "/strings8000_mjj_dijeterr.txt")
strings8500Hist = GetHist(path + "/strings8500_mjj_dijet.txt")
strings8500Error = GetHist(path + "/strings8500_mjj_dijeterr.txt")
strings9000Hist = GetHist(path + "/strings9000_mjj_dijet.txt")
strings9000Error = GetHist(path + "/strings9000_mjj_dijeterr.txt")

strings7000jes1d = GetHist(path + "/systematics/Ms07000GroupedNP_1__1down.txt")
strings7000jes1u = GetHist(path + "/systematics/Ms07000GroupedNP_1__1up.txt")
strings7000jes2d = GetHist(path + "/systematics/Ms07000GroupedNP_2__1down.txt")
strings7000jes2u = GetHist(path + "/systematics/Ms07000GroupedNP_2__1up.txt")
strings7000jes3d = GetHist(path + "/systematics/Ms07000GroupedNP_3__1down.txt")
strings7000jes3u = GetHist(path + "/systematics/Ms07000GroupedNP_3__1up.txt")
strings7000jer1d = GetHist(path + "/systematics/Ms07000JER_EffectiveNP_1__1down.txt")
strings7000jer1u = GetHist(path + "/systematics/Ms07000JER_EffectiveNP_1__1up.txt")
strings7000jer2d = GetHist(path + "/systematics/Ms07000JER_EffectiveNP_2__1down.txt")
strings7000jer2u = GetHist(path + "/systematics/Ms07000JER_EffectiveNP_2__1up.txt")
strings7000jer3d = GetHist(path + "/systematics/Ms07000JER_EffectiveNP_3__1down.txt")
strings7000jer3u = GetHist(path + "/systematics/Ms07000JER_EffectiveNP_3__1up.txt")
strings7000jer4d = GetHist(path + "/systematics/Ms07000JER_EffectiveNP_4__1down.txt")
strings7000jer4u = GetHist(path + "/systematics/Ms07000JER_EffectiveNP_4__1up.txt")
strings7000jer5d = GetHist(path + "/systematics/Ms07000JER_EffectiveNP_5__1down.txt")
strings7000jer5u = GetHist(path + "/systematics/Ms07000JER_EffectiveNP_5__1up.txt")
strings7000jer6d = GetHist(path + "/systematics/Ms07000JER_EffectiveNP_6__1down.txt")
strings7000jer6u = GetHist(path + "/systematics/Ms07000JER_EffectiveNP_6__1up.txt")
strings7000jer7d = GetHist(path + "/systematics/Ms07000JER_EffectiveNP_7restTerm__1do.txt")
strings7000jer7u = GetHist(path + "/systematics/Ms07000JER_EffectiveNP_7restTerm__1up.txt")

strings8500jes1d = GetHist(path + "/systematics/Ms08500GroupedNP_1__1down.txt")
strings8500jes1u = GetHist(path + "/systematics/Ms08500GroupedNP_1__1up.txt")
strings8500jes2d = GetHist(path + "/systematics/Ms08500GroupedNP_2__1down.txt")
strings8500jes2u = GetHist(path + "/systematics/Ms08500GroupedNP_2__1up.txt")
strings8500jes3d = GetHist(path + "/systematics/Ms08500GroupedNP_3__1down.txt")
strings8500jes3u = GetHist(path + "/systematics/Ms08500GroupedNP_3__1up.txt")
strings8500jer1d = GetHist(path + "/systematics/Ms08500JER_EffectiveNP_1__1down.txt")
strings8500jer1u = GetHist(path + "/systematics/Ms08500JER_EffectiveNP_1__1up.txt")
strings8500jer2d = GetHist(path + "/systematics/Ms08500JER_EffectiveNP_2__1down.txt")
strings8500jer2u = GetHist(path + "/systematics/Ms08500JER_EffectiveNP_2__1up.txt")
strings8500jer3d = GetHist(path + "/systematics/Ms08500JER_EffectiveNP_3__1down.txt")
strings8500jer3u = GetHist(path + "/systematics/Ms08500JER_EffectiveNP_3__1up.txt")
strings8500jer4d = GetHist(path + "/systematics/Ms08500JER_EffectiveNP_4__1down.txt")
strings8500jer4u = GetHist(path + "/systematics/Ms08500JER_EffectiveNP_4__1up.txt")
strings8500jer5d = GetHist(path + "/systematics/Ms08500JER_EffectiveNP_5__1down.txt")
strings8500jer5u = GetHist(path + "/systematics/Ms08500JER_EffectiveNP_5__1up.txt")
strings8500jer6d = GetHist(path + "/systematics/Ms08500JER_EffectiveNP_6__1down.txt")
strings8500jer6u = GetHist(path + "/systematics/Ms08500JER_EffectiveNP_6__1up.txt")
strings8500jer7d = GetHist(path + "/systematics/Ms08500JER_EffectiveNP_7restTerm__1do.txt")
strings8500jer7u = GetHist(path + "/systematics/Ms08500JER_EffectiveNP_7restTerm__1up.txt")

strings7500jes1d = GetHist(path + "/systematics/Ms07500GroupedNP_1__1down.txt")
strings7500jes1u = GetHist(path + "/systematics/Ms07500GroupedNP_1__1up.txt")
strings7500jes2d = GetHist(path + "/systematics/Ms07500GroupedNP_2__1down.txt")
strings7500jes2u = GetHist(path + "/systematics/Ms07500GroupedNP_2__1up.txt")
strings7500jes3d = GetHist(path + "/systematics/Ms07500GroupedNP_3__1down.txt")
strings7500jes3u = GetHist(path + "/systematics/Ms07500GroupedNP_3__1up.txt")
strings7500jer1d = GetHist(path + "/systematics/Ms07500JER_EffectiveNP_1__1down.txt")
strings7500jer1u = GetHist(path + "/systematics/Ms07500JER_EffectiveNP_1__1up.txt")
strings7500jer2d = GetHist(path + "/systematics/Ms07500JER_EffectiveNP_2__1down.txt")
strings7500jer2u = GetHist(path + "/systematics/Ms07500JER_EffectiveNP_2__1up.txt")
strings7500jer3d = GetHist(path + "/systematics/Ms07500JER_EffectiveNP_3__1down.txt")
strings7500jer3u = GetHist(path + "/systematics/Ms07500JER_EffectiveNP_3__1up.txt")
strings7500jer4d = GetHist(path + "/systematics/Ms07500JER_EffectiveNP_4__1down.txt")
strings7500jer4u = GetHist(path + "/systematics/Ms07500JER_EffectiveNP_4__1up.txt")
strings7500jer5d = GetHist(path + "/systematics/Ms07500JER_EffectiveNP_5__1down.txt")
strings7500jer5u = GetHist(path + "/systematics/Ms07500JER_EffectiveNP_5__1up.txt")
strings7500jer6d = GetHist(path + "/systematics/Ms07500JER_EffectiveNP_6__1down.txt")
strings7500jer6u = GetHist(path + "/systematics/Ms07500JER_EffectiveNP_6__1up.txt")
strings7500jer7d = GetHist(path + "/systematics/Ms07500JER_EffectiveNP_7restTerm__1do.txt")
strings7500jer7u = GetHist(path + "/systematics/Ms07500JER_EffectiveNP_7restTerm__1up.txt")

strings8000jes1d = GetHist(path + "/systematics/Ms08000GroupedNP_1__1down.txt")
strings8000jes1u = GetHist(path + "/systematics/Ms08000GroupedNP_1__1up.txt")
strings8000jes2d = GetHist(path + "/systematics/Ms08000GroupedNP_2__1down.txt")
strings8000jes2u = GetHist(path + "/systematics/Ms08000GroupedNP_2__1up.txt")
strings8000jes3d = GetHist(path + "/systematics/Ms08000GroupedNP_3__1down.txt")
strings8000jes3u = GetHist(path + "/systematics/Ms08000GroupedNP_3__1up.txt")
strings8000jer1d = GetHist(path + "/systematics/Ms08000JER_EffectiveNP_1__1down.txt")
strings8000jer1u = GetHist(path + "/systematics/Ms08000JER_EffectiveNP_1__1up.txt")
strings8000jer2d = GetHist(path + "/systematics/Ms08000JER_EffectiveNP_2__1down.txt")
strings8000jer2u = GetHist(path + "/systematics/Ms08000JER_EffectiveNP_2__1up.txt")
strings8000jer3d = GetHist(path + "/systematics/Ms08000JER_EffectiveNP_3__1down.txt")
strings8000jer3u = GetHist(path + "/systematics/Ms08000JER_EffectiveNP_3__1up.txt")
strings8000jer4d = GetHist(path + "/systematics/Ms08000JER_EffectiveNP_4__1down.txt")
strings8000jer4u = GetHist(path + "/systematics/Ms08000JER_EffectiveNP_4__1up.txt")
strings8000jer5d = GetHist(path + "/systematics/Ms08000JER_EffectiveNP_5__1down.txt")
strings8000jer5u = GetHist(path + "/systematics/Ms08000JER_EffectiveNP_5__1up.txt")
strings8000jer6d = GetHist(path + "/systematics/Ms08000JER_EffectiveNP_6__1down.txt")
strings8000jer6u = GetHist(path + "/systematics/Ms08000JER_EffectiveNP_6__1up.txt")
strings8000jer7d = GetHist(path + "/systematics/Ms08000JER_EffectiveNP_7restTerm__1do.txt")
strings8000jer7u = GetHist(path + "/systematics/Ms08000JER_EffectiveNP_7restTerm__1up.txt")

strings9000jes1d = GetHist(path + "/systematics/Ms09000GroupedNP_1__1down.txt")
strings9000jes1u = GetHist(path + "/systematics/Ms09000GroupedNP_1__1up.txt")
strings9000jes2d = GetHist(path + "/systematics/Ms09000GroupedNP_2__1down.txt")
strings9000jes2u = GetHist(path + "/systematics/Ms09000GroupedNP_2__1up.txt")
strings9000jes3d = GetHist(path + "/systematics/Ms09000GroupedNP_3__1down.txt")
strings9000jes3u = GetHist(path + "/systematics/Ms09000GroupedNP_3__1up.txt")
strings9000jer1d = GetHist(path + "/systematics/Ms09000JER_EffectiveNP_1__1down.txt")
strings9000jer1u = GetHist(path + "/systematics/Ms09000JER_EffectiveNP_1__1up.txt")
strings9000jer2d = GetHist(path + "/systematics/Ms09000JER_EffectiveNP_2__1down.txt")
strings9000jer2u = GetHist(path + "/systematics/Ms09000JER_EffectiveNP_2__1up.txt")
strings9000jer3d = GetHist(path + "/systematics/Ms09000JER_EffectiveNP_3__1down.txt")
strings9000jer3u = GetHist(path + "/systematics/Ms09000JER_EffectiveNP_3__1up.txt")
strings9000jer4d = GetHist(path + "/systematics/Ms09000JER_EffectiveNP_4__1down.txt")
strings9000jer4u = GetHist(path + "/systematics/Ms09000JER_EffectiveNP_4__1up.txt")
strings9000jer5d = GetHist(path + "/systematics/Ms09000JER_EffectiveNP_5__1down.txt")
strings9000jer5u = GetHist(path + "/systematics/Ms09000JER_EffectiveNP_5__1up.txt")
strings9000jer6d = GetHist(path + "/systematics/Ms09000JER_EffectiveNP_6__1down.txt")
strings9000jer6u = GetHist(path + "/systematics/Ms09000JER_EffectiveNP_6__1up.txt")
strings9000jer7d = GetHist(path + "/systematics/Ms09000JER_EffectiveNP_7restTerm__1do.txt")
strings9000jer7u = GetHist(path + "/systematics/Ms09000JER_EffectiveNP_7restTerm__1up.txt")

strings7000jes1 = Systematic("strings7000jes1",1.0,strings7000jes1u,strings7000jes1d,"user",systype)
strings7000jes2 = Systematic("strings7000jes2",1.0,strings7000jes2u,strings7000jes2d,"user",systype)
strings7000jes3 = Systematic("strings7000jes3",1.0,strings7000jes3u,strings7000jes3d,"user",systype)
strings7000jer1 = Systematic("strings7000jer1",1.0,strings7000jer1u,strings7000jer1d,"user",systype)
strings7000jer2 = Systematic("strings7000jer2",1.0,strings7000jer2u,strings7000jer2d,"user",systype)
strings7000jer3 = Systematic("strings7000jer3",1.0,strings7000jer3u,strings7000jer3d,"user",systype)
strings7000jer4 = Systematic("strings7000jer4",1.0,strings7000jer4u,strings7000jer4d,"user",systype)
strings7000jer5 = Systematic("strings7000jer5",1.0,strings7000jer5u,strings7000jer5d,"user",systype)
strings7000jer6 = Systematic("strings7000jer6",1.0,strings7000jer6u,strings7000jer6d,"user",systype)
strings7000jer7 = Systematic("strings7000jer7",1.0,strings7000jer7u,strings7000jer7d,"user",systype)

strings7500jes1 = Systematic("strings7500jes1",1.0,strings7500jes1u,strings7500jes1d,"user",systype)
strings7500jes2 = Systematic("strings7500jes2",1.0,strings7500jes2u,strings7500jes2d,"user",systype)
strings7500jes3 = Systematic("strings7500jes3",1.0,strings7500jes3u,strings7500jes3d,"user",systype)
strings7500jer1 = Systematic("strings7500jer1",1.0,strings7500jer1u,strings7500jer1d,"user",systype)
strings7500jer2 = Systematic("strings7500jer2",1.0,strings7500jer2u,strings7500jer2d,"user",systype)
strings7500jer3 = Systematic("strings7500jer3",1.0,strings7500jer3u,strings7500jer3d,"user",systype)
strings7500jer4 = Systematic("strings7500jer4",1.0,strings7500jer4u,strings7500jer4d,"user",systype)
strings7500jer5 = Systematic("strings7500jer5",1.0,strings7500jer5u,strings7500jer5d,"user",systype)
strings7500jer6 = Systematic("strings7500jer6",1.0,strings7500jer6u,strings7500jer6d,"user",systype)
strings7500jer7 = Systematic("strings7500jer7",1.0,strings7500jer7u,strings7500jer7d,"user",systype)

strings8000jes1 = Systematic("strings8000jes1",1.0,strings8000jes1u,strings8000jes1d,"user",systype)
strings8000jes2 = Systematic("strings8000jes2",1.0,strings8000jes2u,strings8000jes2d,"user",systype)
strings8000jes3 = Systematic("strings8000jes3",1.0,strings8000jes3u,strings8000jes3d,"user",systype)
strings8000jer1 = Systematic("strings8000jer1",1.0,strings8000jer1u,strings8000jer1d,"user",systype)
strings8000jer2 = Systematic("strings8000jer2",1.0,strings8000jer2u,strings8000jer2d,"user",systype)
strings8000jer3 = Systematic("strings8000jer3",1.0,strings8000jer3u,strings8000jer3d,"user",systype)
strings8000jer4 = Systematic("strings8000jer4",1.0,strings8000jer4u,strings8000jer4d,"user",systype)
strings8000jer5 = Systematic("strings8000jer5",1.0,strings8000jer5u,strings8000jer5d,"user",systype)
strings8000jer6 = Systematic("strings8000jer6",1.0,strings8000jer6u,strings8000jer6d,"user",systype)
strings8000jer7 = Systematic("strings8000jer7",1.0,strings8000jer7u,strings8000jer7d,"user",systype)

strings8500jes1 = Systematic("strings8500jes1",1.0,strings8500jes1u,strings8500jes1d,"user",systype)
strings8500jes2 = Systematic("strings8500jes2",1.0,strings8500jes2u,strings8500jes2d,"user",systype)
strings8500jes3 = Systematic("strings8500jes3",1.0,strings8500jes3u,strings8500jes3d,"user",systype)
strings8500jer1 = Systematic("strings8500jer1",1.0,strings8500jer1u,strings8500jer1d,"user",systype)
strings8500jer2 = Systematic("strings8500jer2",1.0,strings8500jer2u,strings8500jer2d,"user",systype)
strings8500jer3 = Systematic("strings8500jer3",1.0,strings8500jer3u,strings8500jer3d,"user",systype)
strings8500jer4 = Systematic("strings8500jer4",1.0,strings8500jer4u,strings8500jer4d,"user",systype)
strings8500jer5 = Systematic("strings8500jer5",1.0,strings8500jer5u,strings8500jer5d,"user",systype)
strings8500jer6 = Systematic("strings8500jer6",1.0,strings8500jer6u,strings8500jer6d,"user",systype)
strings8500jer7 = Systematic("strings8500jer7",1.0,strings8500jer7u,strings8500jer7d,"user",systype)

strings9000jes1 = Systematic("strings9000jes1",1.0,strings9000jes1u,strings9000jes1d,"user",systype)
strings9000jes2 = Systematic("strings9000jes2",1.0,strings9000jes2u,strings9000jes2d,"user",systype)
strings9000jes3 = Systematic("strings9000jes3",1.0,strings9000jes3u,strings9000jes3d,"user",systype)
strings9000jer1 = Systematic("strings9000jer1",1.0,strings9000jer1u,strings9000jer1d,"user",systype)
strings9000jer2 = Systematic("strings9000jer2",1.0,strings9000jer2u,strings9000jer2d,"user",systype)
strings9000jer3 = Systematic("strings9000jer3",1.0,strings9000jer3u,strings9000jer3d,"user",systype)
strings9000jer4 = Systematic("strings9000jer4",1.0,strings9000jer4u,strings9000jer4d,"user",systype)
strings9000jer5 = Systematic("strings9000jer5",1.0,strings9000jer5u,strings9000jer5d,"user",systype)
strings9000jer6 = Systematic("strings9000jer6",1.0,strings9000jer6u,strings9000jer6d,"user",systype)
strings9000jer7 = Systematic("strings9000jer7",1.0,strings9000jer7u,strings9000jer7d,"user",systype)

bkgHistd = []
bkgHistu = []
for i in range(len(bkgHist)):
        if bkgHist[i] == 0:
                bkgHistd.append(0)
                bkgHistu.append(0)
        else:
                bkgHistd.append(bkgHist[i]-(bkgError[i]/bkgHist[i]))
                bkgHistu.append(bkgHist[i]+(bkgError[i]/bkgHist[i]))

bkgsys = Systematic("bkg_stat",1.0,bkgHistu,bkgHistd,"user",systype)

#--------------
# Build Samples
#--------------

bkgSample = Sample("Bkg",kGreen-9)
bkgSample.buildHisto(bkgHist, "SR", "mjj", BINLOW, BINWIDTH)
#bkgSample.buildStatErrors(bkgError, "SR", "mjj")
#bkgSample.setStatConfig(True)
bkgSample.addSystematic(bkgsys)

dataSample = Sample("Data",kBlack)
dataSample.setData()
dataSample.buildHisto(dataHist, "SR", "mjj", BINLOW, BINWIDTH)
dataSample.buildStatErrors(dataError, "SR", "mjj")

#**************
# Exclusion fit
#**************
if myFitType==FitType.Exclusion:
	for Ms in sigSamples:
		exclusionFitConfig = configMgr.addFitConfig("Exclusion_"+Ms)
		meas = exclusionFitConfig.addMeasurement(name="NormalMeasurement",lumi=1.0,lumiErr=0.017)
    		meas.addPOI("mu_SIG")

    		exclusionFitConfig.addSamples([bkgSample,dataSample])

    		srBin = exclusionFitConfig.addChannel("mjj",["SR"], nBins, BINLOW, BINLOW + nBins*BINWIDTH)
    		srBin.useOverflowBin  = True
    		srBin.useUnderflowBin = True

    		#srBin.hasStatConfig=True
    		#srBin.statErrorThreshold=0.05
    		#srBin.statErrorType = "Gaussian"

		exclusionFitConfig.addSignalChannels([srBin])

		stringsSample = Sample(Ms, kMagenta)
		stringsSample.setNormByTheory()
		stringsSample.setStatConfig(True)
		stringsSample.setNormFactor("mu_SIG",1.0,0.0,1.0)

		if Ms == "Ms7":
			stringsSample.buildHisto(strings7000Hist, "SR", "mjj", BINLOW, BINWIDTH)
			stringsSample.buildStatErrors(strings7000Error, "SR", "mjj")
			stringsSample.addSystematic(strings7000jes1)
			stringsSample.addSystematic(strings7000jes2)
			stringsSample.addSystematic(strings7000jes3)
			stringsSample.addSystematic(strings7000jer1)
			stringsSample.addSystematic(strings7000jer2)
			stringsSample.addSystematic(strings7000jer3)
			stringsSample.addSystematic(strings7000jer4)
			stringsSample.addSystematic(strings7000jer5)
			stringsSample.addSystematic(strings7000jer6)
			stringsSample.addSystematic(strings7000jer7)
		elif Ms == "Ms7.5":
                        stringsSample.buildHisto(strings7500Hist, "SR", "mjj", BINLOW, BINWIDTH)
                        stringsSample.buildStatErrors(strings7500Error, "SR", "mjj")
                        stringsSample.addSystematic(strings7500jes1)
                        stringsSample.addSystematic(strings7500jes2)
                        stringsSample.addSystematic(strings7500jes3)
                        stringsSample.addSystematic(strings7500jer1)
                        stringsSample.addSystematic(strings7500jer2)
                        stringsSample.addSystematic(strings7500jer3)
                        stringsSample.addSystematic(strings7500jer4)
                        stringsSample.addSystematic(strings7500jer5)
                        stringsSample.addSystematic(strings7500jer6)
                        stringsSample.addSystematic(strings7500jer7)
		elif Ms == "Ms8":
                        stringsSample.buildHisto(strings8000Hist, "SR", "mjj", BINLOW, BINWIDTH)
                        stringsSample.buildStatErrors(strings8000Error, "SR", "mjj")
                        stringsSample.addSystematic(strings8000jes1)
                        stringsSample.addSystematic(strings8000jes2)
                        stringsSample.addSystematic(strings8000jes3)
                        stringsSample.addSystematic(strings8000jer1)
                        stringsSample.addSystematic(strings8000jer2)
                        stringsSample.addSystematic(strings8000jer3)
                        stringsSample.addSystematic(strings8000jer4)
                        stringsSample.addSystematic(strings8000jer5)
                        stringsSample.addSystematic(strings8000jer6)
                        stringsSample.addSystematic(strings8000jer7)
		elif Ms == "Ms8.5":
                        stringsSample.buildHisto(strings8500Hist, "SR", "mjj", BINLOW, BINWIDTH)
                        stringsSample.buildStatErrors(strings8500Error, "SR", "mjj")
                        stringsSample.addSystematic(strings8500jes1)
                        stringsSample.addSystematic(strings8500jes2)
                        stringsSample.addSystematic(strings8500jes3)
                        stringsSample.addSystematic(strings8500jer1)
                        stringsSample.addSystematic(strings8500jer2)
                        stringsSample.addSystematic(strings8500jer3)
                        stringsSample.addSystematic(strings8500jer4)
                        stringsSample.addSystematic(strings8500jer5)
                        stringsSample.addSystematic(strings8500jer6)
                        stringsSample.addSystematic(strings8500jer7)
		elif Ms == "Ms9":
                        stringsSample.buildHisto(strings9000Hist, "SR", "mjj", BINLOW, BINWIDTH)
                        stringsSample.buildStatErrors(strings9000Error, "SR", "mjj")
                        stringsSample.addSystematic(strings9000jes1)
                        stringsSample.addSystematic(strings9000jes2)
                        stringsSample.addSystematic(strings9000jes3)
                        stringsSample.addSystematic(strings9000jer1)
                        stringsSample.addSystematic(strings9000jer2)
                        stringsSample.addSystematic(strings9000jer3)
                        stringsSample.addSystematic(strings9000jer4)
                        stringsSample.addSystematic(strings9000jer5)
                        stringsSample.addSystematic(strings9000jer6)
                        stringsSample.addSystematic(strings9000jer7)
    		
		exclusionFitConfig.addSamples(stringsSample)
    		exclusionFitConfig.setSignalSample(stringsSample)
    		srBin.logY = True
