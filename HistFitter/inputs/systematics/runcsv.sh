# Loop over 5 signal samples.
for sample in $(seq 0 4)
do
  # Loop over monimal and 2X10 systematics.
  for sys in $(seq 0 19)
  do
    root -l -b -q "makecsv.C($sample,$sys)"
  done;
done;
