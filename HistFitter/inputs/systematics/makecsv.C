Int_t makecsv(const Int_t sample, const Int_t sys)
{
	const TString path = "/hepusers2/rflyons/code/DijetFiles/systematics/";

	// Samples
	const TString signals[5] = {"Ms07000", "Ms07500", "Ms08000", "Ms08500", "Ms09000"};
	
	// Systematics
	const TString systematics[] = {"GroupedNP_1__1up",
                            		"GroupedNP_1__1down",
                            		"GroupedNP_2__1up",
                            		"GroupedNP_2__1down",
                            		"GroupedNP_3__1up",
                            		"GroupedNP_3__1down",
                            		"JER_EffectiveNP_1__1up", 
                            		"JER_EffectiveNP_1__1down",
                            		"JER_EffectiveNP_2__1up",
                            		"JER_EffectiveNP_2__1down",
                            		"JER_EffectiveNP_3__1up",
                            		"JER_EffectiveNP_3__1down",
                            		"JER_EffectiveNP_4__1up",
                            		"JER_EffectiveNP_4__1down",
                            		"JER_EffectiveNP_5__1up",
                            		"JER_EffectiveNP_5__1down",
                            		"JER_EffectiveNP_6__1up",
                            		"JER_EffectiveNP_6__1down",
                            		"JER_EffectiveNP_7restTerm__1up",
                            		"JER_EffectiveNP_7restTerm__1do"};

	TString fileNamenom = signals[sample] + "Nominal.root";
	TString objectNamenom = "mjj_" + signals[sample] + "Nominal";
	TString fileName = signals[sample] + systematics[sys] + ".root";
	TString objectName = "mjj_" + signals[sample] + systematics[sys];

	ofstream fileOut;
	fileOut.open(objectName(4,40) + ".txt",ios::out); 
	if (!fileOut.is_open()) {
		std::cout << "Error opening output file" << std::endl;
		return 1;
	}

	TFile *fileInnom = new TFile(path+fileNamenom);
	if (fileInnom == NULL) {
		std::cout << "Error openining input file " + path + fileNamenom << std::endl;
		return 2;
	}
	TH1D *hnom = (TH1D*)fileInnom->Get(objectNamenom);
		if (hnom == NULL) {
		std::cout << "Can not find histogram objects " + objectNamenom << std::endl;
		return 3;
	}

        TFile *fileIn = new TFile(path+fileName);
        if (fileIn == NULL) {
                std::cout << "Error openining input file " + path + fileName << std::endl;
                return 2;
        }
        TH1D *h = (TH1D*)fileIn->Get(objectName);
                if (h == NULL) {
                std::cout << "Can not find histogram objects " + objectName << std::endl;
                return 3;
        }

	const Int_t Nbins = hnom->GetNbinsX();
	cout << Nbins << " bins found" << endl;
	Double_t value;
	for (Int_t i=1;i<Nbins;i++) {
		if (hnom->GetBinContent(i) == 0) {
			value = 0;
		}
		else {
			value = h->GetBinContent(i) / hnom->GetBinContent(i);
		}
		fileOut << value << ",";
	}
	if (hnom->GetBinContent(Nbins) == 0) {
		value = 0;
        }
        else {
                value = h->GetBinContent(Nbins) / hnom->GetBinContent(Nbins);
        }
	fileOut << value << endl;
	
	std::cout << "normal sucessful completion" << std::endl;
	std::cout << "Processed " + fileName << std::endl;
	return 0;
}
