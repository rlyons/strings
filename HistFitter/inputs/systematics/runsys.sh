#
rm jes.dat
rm Ms*
#
# Loop over 5 signal samples.
for sample in $(seq 0 4)
do
  # Loop over monimal and 2X10 systematics.
  for sys in $(seq 0 20)
  do
    root -l -b -q "makesystematic.C($sample,$sys)"
  done;
done;

