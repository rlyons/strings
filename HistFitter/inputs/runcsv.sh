rm *.txt
# bkg hist and error.
root -l -b -q "makecsv.C(0,0)"

# data hist.
root -l -b -q "makecsv.C(1,0)"

# Loop over 5 signal samples.
for sample in $(seq 0 4)
do
  root -l -b -q "makecsv.C(2,$sample)"
done;
