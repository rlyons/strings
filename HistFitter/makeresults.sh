rm -r results/mjj_dijet_test/Ms*
for sample in Ms7 Ms7.5 Ms8 Ms8.5 Ms9
do
	mkdir results/mjj_dijet_test/${sample}
	
	HistFitter.py -t -w -f -F excl -D "before,after" -g "$sample" -l dijet_test.py > results/mjj_dijet_test/${sample}/log.out
	
	mv results/mjj_dijet_test_Output* results/mjj_dijet_test/${sample}
	mv results/${sample}_Asym_CLs_grid_ts3.root results/mjj_dijet_test/${sample}	
	mv results/upperlimit_cls_poi* results/mjj_dijet_test/${sample}

	mv results/mjj_dijet_test/fit_parameters.root results/mjj_dijet_test/${sample}
	mv results/mjj_dijet_test/SR_mjj* results/mjj_dijet_test/${sample}
	mv results/mjj_dijet_test/Exclusion* results/mjj_dijet_test/${sample}
done
